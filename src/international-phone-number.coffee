# Author Marek Pietrucha
# https://github.com/mareczek/international-phone-number

"use strict"
angular.module("internationalPhoneNumber", []).directive 'internationalPhoneNumber', ['$timeout', ($timeout) ->

  restrict:   'A'
  require: '^ngModel'
  scope: {
    ngModel: '='
    defaultCountry: '@'
  }
  link: {
    post: (scope, element, attrs, ctrl) ->
      options =
        autoFormat: true
        autoHideDialCode: true
        defaultCountry: ''
        nationalMode: false
        numberType: ''
        onlyCountries: undefined
        preferredCountries: ['de', 'nl', 'fr', 'es', 'ch']
        responsiveDropdown: false
        utilsScript: ""
  
      unless attrs.skipUtilScriptDownload != undefined || options.utilsScript
        element.intlTelInput('loadUtils', '/bower_components/intl-tel-input/lib/libphonenumber/build/utils.js')

      read = () ->
        ctrl.$setViewValue element.val()

      handleWhatsSupposedToBeAnArray = (value) ->
        if value instanceof Array
          value
        else
          value.toString().replace(/[ ]/g, '').split(',')

      angular.forEach options, (value, key) ->
        return unless attrs.hasOwnProperty(key) and angular.isDefined(attrs[key])
        option = attrs[key]
        if key == 'preferredCountries'
          options.preferredCountries = handleWhatsSupposedToBeAnArray option
        else if key == 'onlyCountries'
          options.onlyCountries = handleWhatsSupposedToBeAnArray option
        else if typeof(value) == "boolean"
          options[key] = (option == "true")
        else
          options[key] = option
          
      element.intlTelInput(options)

      ctrl.$formatters.push (value) ->
        if !value
          return value
        else
          element.intlTelInput 'setNumber', value
          return element.val()

      ctrl.$parsers.push (value) ->
        return value if !value
        return value.replace(/[\s]/g, '');

      ctrl.$validators.internationalPhoneNumber = (value) ->
        if !value
          return value
        else
          return element.intlTelInput("isValidNumber")


      element.on 'blur keyup change', (event) ->
        scope.$apply read

      element.on '$destroy', () ->
        element.intlTelInput('destroy');
        element.off 'blur keyup change'
  }
]
